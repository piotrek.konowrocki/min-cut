using MinCutLib;
using System;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace MinCutTests
{
    public class CorrectnessTests
    {
        int ComputeEdgeConnectivity(List<Node> nodes, List<Edge> edgesSource)
        {
            var edges = edgesSource.Select(x => x).ToList();
            edges = edges.Concat(edges.Select(e => e.Reverse)).ToList();
            edges.ForEach(e => e.U.Edges.Add(e));
            return MinCutSolve.CalculateMinCut(nodes, edges);
        }


        [Theory]
        [InlineData(1)]
        [InlineData(2)]
        [InlineData(3)]
        [InlineData(10)]
        [InlineData(100)]
        public void EmptyGraph(int n)
        {
            var nodes = Enumerable.Range(0, n).Select(i => new Node(i)).ToList();
            var edges = new List<Edge>();
            var edgeConnectivity = ComputeEdgeConnectivity(nodes, edges);
            Assert.Equal(0, edgeConnectivity);
        }

        [Theory]
        [InlineData(2)]
        [InlineData(3)]
        [InlineData(10)]
        [InlineData(100)]
        public void CycleGraph(int n)
        {
            var nodes = Enumerable.Range(0, n).Select(i => new Node(i)).ToList();
            var edges = Enumerable.Range(0, n - 1).Select(i => new Edge(nodes[i], nodes[i + 1])).Append(new Edge(nodes[n - 1], nodes[0])).ToList();
            var edgeConnectivity = ComputeEdgeConnectivity(nodes, edges);
            Assert.Equal(2, edgeConnectivity);
        }

        [Theory]
        [InlineData(2)]
        [InlineData(3)]
        [InlineData(4)]
        [InlineData(5)]
        [InlineData(10)]
        [InlineData(20)]
        public void CompleteGraph(int n)
        {
            var nodes = Enumerable.Range(0, n).Select(i => new Node(i)).ToList();
            var edges = Enumerable.Range(0, n).SelectMany(i =>
            {
                return Enumerable.Range(0, i).Select(j => new Edge(nodes[i], nodes[j]));
            }).ToList();
            var edgeConnectivity = ComputeEdgeConnectivity(nodes, edges);
            Assert.Equal(Math.Max(1, n - 1), edgeConnectivity);
        }

        [Theory]
        [InlineData(2)]
        [InlineData(3)]
        [InlineData(5)]
        [InlineData(10)]
        [InlineData(20)]
        [InlineData(50)]
        public void RandomGraphMinDegreeTest(int n)
        {
            var rangeCount = 50;
            var densityRange = Enumerable.Range(0, rangeCount).Select(number => .5d - .5d * Math.Cos(Math.PI * ((2 * number + 1) / (2d * rangeCount))));
            foreach (var density in densityRange)
            {
                InstanceGeneratorService.GenerateRandomInstance(n, density, out var nodes, out var edges);

                var minDegree = nodes.Min(node => edges.Count(edge => edge.U.N == node.N));

                Assert.True(minDegree >= MinCutSolve.CalculateMinCut(nodes, edges));
            }
        }


        [Fact]
        public void PetersenGraph()
        {
            var nodes = Enumerable.Range(0, 10).Select(i => new Node(i)).ToList();
            var edges = new List<Edge>();


            for (int i = 0; i < 5; i += 1)
            {
                edges.Add(new Edge(nodes[i], nodes[(i + 1) % 5]));
                edges.Add(new Edge(nodes[i], nodes[i + 5]));
            }

            edges.Add(new Edge(nodes[5], nodes[7]));
            edges.Add(new Edge(nodes[5], nodes[8]));
            edges.Add(new Edge(nodes[6], nodes[8]));
            edges.Add(new Edge(nodes[6], nodes[9]));
            edges.Add(new Edge(nodes[7], nodes[9]));
            var edgeConnectivity = ComputeEdgeConnectivity(nodes, edges);
            Assert.Equal(3, edgeConnectivity);
        }
    }
}
