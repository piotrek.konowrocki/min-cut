﻿using EasyConsole;
using MinCutLib;
using System;
using System.Diagnostics;

namespace MinCut
{
    class Program
    {
        static Menu menu;

        static System.ConsoleColor infoColor = ConsoleColor.Gray;

        static System.ConsoleColor progressColor = ConsoleColor.Green;
        static System.ConsoleColor resultColor = ConsoleColor.Yellow;

        static void Main(string[] args)
        {
            void Solve()
            {
                Console.WriteLine();
                Output.WriteLine(infoColor, "Input filename:");
                var fileName = Input.ReadString(">");

                var graph = GraphReaderService.ReadFile(fileName);
                var nodes = graph.Item1;
                var edges = graph.Item2;

                Output.WriteLine(progressColor, "1/3 Read file");
                Output.WriteLine(progressColor, "2/3 Started calculations");

                Stopwatch sw = Stopwatch.StartNew();
                int minCut = MinCutSolve.CalculateMinCut(nodes, edges);
                sw.Stop();

                Output.WriteLine(resultColor, $"Edge connectivity of graph: {minCut}");

                GraphReaderService.SaveFile(fileName, minCut);

                Output.WriteLine(progressColor, "3/3 Saved answer");
                Output.WriteLine(progressColor, $"Done in {sw.Elapsed.TotalMilliseconds}ms.");
                Console.WriteLine();
                menu?.Display();
            }

            void Generate()
            {
                Console.WriteLine();
                Output.WriteLine(infoColor, "Input filename:");
                var fileName = Input.ReadString(">");

                Output.WriteLine(infoColor, "Input number of nodes:");
                var nodes = Input.ReadInt(">", 1, Int32.MaxValue);

                Output.WriteLine(infoColor, "Input percent of edges:");
                Console.Write("> ");
                double p = Double.Parse(Console.ReadLine());

                void ProgressWriter(object sender, string e)
                {
                    Output.WriteLine(progressColor, e);
                }

                InstanceGeneratorService.eventHandler += ProgressWriter;
                InstanceGeneratorService.GenerateInstance(nodes, p, fileName);
                InstanceGeneratorService.eventHandler -= ProgressWriter;

                Console.WriteLine();
                menu?.Display();
            }

            void Generate2()
            {
                Console.WriteLine();
                Output.WriteLine(infoColor, "Input filename:");
                var fileName = Input.ReadString(">");

                Output.WriteLine(infoColor, "Input number of nodes:");
                var nodes = Input.ReadInt(">", 1, Int32.MaxValue);

                Output.WriteLine(infoColor, "Input probability of edge:");
                Console.Write("> ");
                double p = Double.Parse(Console.ReadLine());

                void ProgressWriter(object sender, string e)
                {
                    Output.WriteLine(progressColor, e);
                }

                InstanceGeneratorService.eventHandler += ProgressWriter;
                InstanceGeneratorService.GenerateInstance2(nodes, p, fileName);
                InstanceGeneratorService.eventHandler -= ProgressWriter;

                Console.WriteLine();
                menu?.Display();
            }

            menu = new Menu()
                .Add("Solve mincut problem", () => Solve())
                .Add("Generate problem instance (slower, assured connectivity in graph)", () => Generate())
                .Add("Generate problem instance (faster, based on probability, connectivity not assured)", () => Generate2());
            menu.Display();
        }
    }
}
