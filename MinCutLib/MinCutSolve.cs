﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace MinCutLib
{
    public static class MinCutSolve
    {
        public static int CalculateMinCut(List<Node> nodes, List<Edge> edges)
        {
            if (nodes.Count == 1)
            {
                return 0;
            }
            int maxFlow = Int32.MaxValue;
            Node v = nodes.First();
            foreach(Node u in nodes.Except(new Node[] { v }))
            {
                int flow = EdmondsKarp.CalculateMaxFlow(nodes, v, u);
                edges.ForEach(e => e.Flow = 0);
                if(flow < maxFlow)
                {
                    maxFlow = flow;
                }
            }
            return maxFlow;
        }
    }
}