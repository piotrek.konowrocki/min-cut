﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace MinCutLib
{
    public static class GraphReaderService
    {
        public static Tuple<List<Node>, List<Edge>> ReadFile(string fileName)
        {
            using(StreamReader streamReader = new StreamReader(fileName))
            {
                int n = Int32.Parse(streamReader.ReadLine());
                List<Node> nodes = new List<Node>();
                for(int i=0; i < n; i++)
                    nodes.Add(new Node(i));

                List<Edge> edges = streamReader.ReadToEnd()
                    .Split('\n').Select(ss => ss.Split(','))
                    .Select(ss => ss.Select(s=>Int32.Parse(s)))
                    .Select(ii => new Edge(
                        nodes[ii.First()], nodes[ii.Last()]))
                    .ToList();
                edges = edges.Concat(edges.Select(e => e.Reverse)).ToList();
                edges.ForEach(e => e.U.Edges.Add(e));
                return new Tuple<List<Node>, List<Edge>>(nodes,edges);
            }
        }

        public static void SaveFile(string fileName, int minCut)
        {
            using (StreamWriter streamWriter = File.CreateText(fileName+".edgeconnectivity"))
            {
                streamWriter.WriteLine(minCut);
            }
        }

        public static void SaveInstance(string fileName, int n, List<Tuple<int,int>> edges)
        {
            using (StreamWriter streamWriter = File.CreateText(fileName))
            {
                streamWriter.Write(n);
                foreach(var e in edges)
                {
                    streamWriter.WriteLine("");
                    streamWriter.Write($"{e.Item1},{e.Item2}");
                }
            }
        }
    }
}
