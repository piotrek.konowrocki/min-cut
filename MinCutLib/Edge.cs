﻿namespace MinCutLib
{
    public class Edge
    {
        public Node U { get; }
        public Node V { get; }
        public int Capacity { get; }
        public int Flow { get; set; }
        public Edge Reverse { get; set; }
        public Edge(Node u, Node v) : this(u, v, 1)
        {
            Reverse = new Edge(v, u, 1) { Reverse = this};
        }
        private Edge(Node u, Node v, int capacity)
        {
            U = u;
            V = v;
            Capacity = capacity;
            Flow = 0;
        }
    }
}
