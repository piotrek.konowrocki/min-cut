﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace MinCutLib
{
    public static class InstanceGeneratorService
    {
        public static EventHandler<string> eventHandler;
        public static void GenerateInstance(int n, double p, string fileName)
        {
            int fullyConnected = n * (n - 1) / 2;
            int numberOfEdges = (int)(p * fullyConnected);
            List<int> vertices = new List<int>() { 0 };
            List<Tuple<int, int>> edges = new List<Tuple<int, int>>();
            List<Tuple<int, int>> fullyConnectedEdges = new List<Tuple<int, int>>();
            for (int i = 0; i < n; i++)
            {
                for (int j = i + 1; j < n; j++)
                {
                    fullyConnectedEdges.Add(new Tuple<int, int>(i, j));
                }
            }

            eventHandler?.Invoke(null, "1/4 Built fully connected graph");

            var random = new Random();
            for (int i = 1; i < n; i++)
            {
                int index = random.Next(vertices.Count);
                edges.Add(new Tuple<int, int>(vertices[index], i));
                vertices.Add(i);
                fullyConnectedEdges.Remove(new Tuple<int, int>(vertices[index], i));
            }

            eventHandler?.Invoke(null, "2/4 Assured connectivity in graph");

            int restOfEdges = numberOfEdges - (n - 1);

            if (restOfEdges > 0)
            {
                var indexesOfEdges = Enumerable.Range(0, fullyConnectedEdges.Count).OrderBy(x => new Random(x).Next()).Take(restOfEdges);
                edges.AddRange(indexesOfEdges.Select(index => fullyConnectedEdges[index]));

                eventHandler?.Invoke(null, "3/4 Assured percent of edges");

                GraphReaderService.SaveInstance(fileName, n, edges);

                eventHandler?.Invoke(null, "4/4 Saved to file");
            }
            else
            {
                eventHandler?.Invoke(null, "3/4 Assured percent of edges");

                GraphReaderService.SaveInstance(fileName, n, edges);

                eventHandler?.Invoke(null, "4/4 Saved to file");
            }
        }

        public static void GenerateInstance2(int n, double p, string fileName)
        {
            eventHandler?.Invoke(null, "1/3 Started building graph");
            var random = new Random();
            using (StreamWriter streamWriter = File.CreateText(fileName))
            {
                streamWriter.Write(n);
                for (int i = 0; i < n; i++)
                {
                    for (int j = i + 1; j < n; j++)
                    {
                        if (j != i && random.NextDouble() <= p)
                        {
                            streamWriter.WriteLine("");
                            streamWriter.Write($"{i},{j}");
                        }
                    }
                }
            }
            eventHandler?.Invoke(null, "2/3 Graph built");
            eventHandler?.Invoke(null, "3/3 Saved to file");
        }

        public static void GenerateRandomInstance(int n, double p, out List<Node> nodes, out List<Edge> edges)
        {
            var random = new Random();
            nodes = Enumerable.Range(0, n).Select(i => new Node(i)).ToList();
            edges = new List<Edge>();
            for (int i = 0; i < n; i++)
            {
                for (int j = i + 1; j < n; j++)
                {
                    if (j != i && random.NextDouble() <= p)
                    {
                        edges.Add(new Edge(nodes[i], nodes[j]));
                    }
                }
            }
            edges = edges.Concat(edges.Select(e => e.Reverse)).ToList();
            edges.ForEach(e => e.U.Edges.Add(e));
        }
    }
}