﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace MinCutLib
{
    public static class EdmondsKarp
    {
        public static int CalculateMaxFlow(List<Node> nodes, Node source, Node sink)
        {
            int maxFlow = 0;
            while (true)
            {
                Dictionary<Node, Edge> parent = new Dictionary<Node, Edge>();
                List<Node> q = new List<Node>
                {
                    source
                };

                while (q.Count != 0)
                {
                    Node curr = q.First();
                    q.Remove(curr);
                    foreach(Edge e in curr.Edges)
                        if(!parent.ContainsKey(e.V) && e.V != source && e.Capacity > e.Flow)
                        {
                            parent.Add(e.V, e);
                            q.Add(e.V);
                        }
                }
                if (!parent.ContainsKey(sink))
                    break;

                int pushFlow = Int32.MaxValue;
                for (Edge e = parent[sink]; e != null; e = parent.ContainsKey(e.U) ? parent[e.U] : null)
                    pushFlow = Math.Min(pushFlow, e.Capacity - e.Flow);

                for(Edge e = parent[sink]; e!=null; e = parent.ContainsKey(e.U) ? parent[e.U] : null)
                {
                    e.Flow += pushFlow;
                    e.Reverse.Flow -= pushFlow;
                }

                maxFlow += pushFlow;
            }

            return maxFlow;
        }
    }
}