﻿using System.Collections.Generic;
using System.Linq;

namespace MinCutLib
{
    public class Node
    {
        public int N { get; }
        public List<Edge> Edges { get; set; }
        public Node(int node)
        {
            N = node;
            Edges = new List<Edge>();
        }
    }

    public static class NodeExtension
    {
        public static Node FindNode(this List<Node> nodes, int n) => nodes.Where(x => x.N == n).FirstOrDefault();
    }
}
